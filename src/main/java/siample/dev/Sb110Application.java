package siample.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;


@SpringBootApplication
public class Sb110Application {	
	
	@Bean(name="messPOJO")
	@Profile("dev")
	public Message giveDevMessage() {
		return new Message("dev profile message");
	}	
	
	@Bean(name="messPOJO")
	@Profile("prod")
	public Message giveProdMessage() {
		return new Message("prod profile message");
	}
	
	public static void main(String[] args) {
		ApplicationContext aContext = SpringApplication.run(Sb110Application.class, args);
		System.out.println(aContext.getBean("messPOJO"));
	}

}
